#include <iostream>
#include <conio.h>

//Error occurs when trying to assign the element of vector from index 8
void index_out_of_bounds()
{
	int myVector[8];

	for (int index = 0; index < 8; index++)
	{
		myVector[index] = index + 1;
		std::cout << myVector[index] << " -> ";
	}	
}

//No error occurs, but this is a bug because the "if" instruction will be never reached
int dead_code(int a, int b)
{
	if (a < b)
	{
		return 0;
	}

	return a - b;
}

//No error occurs, but the output of this function will not be something expected because myVariable is not initialized, so it will have a random value
void missing_initialization(int a)
{
	int myVariable = 10;

	std::cout << a / myVariable;
}

//Our string has 4 elements, but the last element should be NULL, respresenting the end of the string. The output will not be something expected
void missing_terminator()
{
	char myString[4];
	int index = 0;

	myString[index++] = 'a';
	myString[index++] = 'b';
	myString[index++] = 'c';
	myString[index++] = NULL;

	std::cout << myString;
}

//This kind of bugs are usually caused by accident, rather than misunderstanding. In the last line we should have "=" instad of "==", therefore it will have no effect
void no_effect()
{
	int a = 5, b = 8, aux;

	aux = a;
	a = b;
	b = aux;
	
	std::cout << a << " " << b << std::endl;
}

void main()
{
	index_out_of_bounds();
	dead_code(5, 6);
	missing_initialization(6);
	missing_terminator();
	no_effect();
	
	std::cout << "Hello!";
	_getch();
}